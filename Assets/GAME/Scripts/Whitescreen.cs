﻿
// Andrés Villalobos | twitter.com/matnesis | andresalvivar@gmail.com
// 2017/06/27 07:42 pm


using UnityEngine;

public class Whitescreen : MonoBehaviour
{
    public Renderer render;

    private static Whitescreen self;
    public static Whitescreen Instance
    {
        get
        {
            if (self == null) self = FindObjectOfType<Whitescreen>();
            return self;
        }
    }
}
