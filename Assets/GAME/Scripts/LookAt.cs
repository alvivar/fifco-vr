﻿
// @matnesis
// 2017/06/27 04:22 pm


using UnityEngine;

public class LookAt : MonoBehaviour
{
    public Transform target;
    public Vector3 eulerOffset;


    [ContextMenu("Use Player Tag as Target")]
    void UsePlayerTagAsTarget()
    {
        var p = GameObject.FindGameObjectWithTag("Player");
        if (p) target = p.transform;
    }

    void Update()
    {
        if (target != null)
        {
            var pos = target.position;
            transform.LookAt(pos);
            transform.localEulerAngles += eulerOffset;
        }
    }
}
