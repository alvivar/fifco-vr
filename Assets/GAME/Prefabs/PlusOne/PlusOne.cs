﻿
// Andrés Villalobos | twitter.com/matnesis | andresalvivar@gmail.com
// 2017/06/27 08:19 pm


using UnityEngine;
using DG.Tweening;

public class PlusOne : MonoBehaviour
{
    [Header("Required")]
    public TextMesh text;


    private static PlusOne self;
    public static PlusOne Instance
    {
        get
        {
            if (self == null) self = FindObjectOfType<PlusOne>();
            return self;
        }
    }


    public void Show(Vector3 pos)
    {
        transform.position = pos;

        DOTween
            .Sequence()
            .Append(DOTween.To(() => text.color, x => text.color = x, Color.white, 0.10f))
            .Append(DOTween.To(() => text.color, x => text.color = x, Color.clear, 4));
    }
}
