﻿
// Andrés Villalobos | twitter.com/matnesis | andresalvivar@gmail.com
// 2017/06/27 05:12 pm


using UnityEngine;
using matnesis.TeaTime;
using DG.Tweening;

public class PlayButton : MonoBehaviour
{
    public int score = 0;

    [Header("Config")]
    public int gameDuration = 30;

    [Header("Children Required")]
    public TextMesh message;
    public Transform button;
    public Renderer buttonBg;
    public TextMesh buttonText;
    public TextMesh hiscoreText;

    private Vector3 buttonScale;
    private bool gameStarted = false;
    private int gameTime = 0;


    private static PlayButton self;
    public static PlayButton Instance
    {
        get
        {
            if (self == null) self = FindObjectOfType<PlayButton>();
            return self;
        }
    }


    void Start()
    {
        buttonScale = button.localScale;


        // Game Cycle
        {
            var originalDuration = gameDuration;
            this.tt("Game").Pause().Add(t =>
            {
                buttonText.text = gameDuration + "";
                buttonText.transform.DOShakeScale(0.9f, 0.2f);

                gameDuration -= 1;

                if (gameDuration <= 0)
                {
                    // Flash
                    DOTween
                        .Sequence()
                        .Append(Whitescreen.Instance.render.material.DOColor(Color.white, 0.1f))
                        .Append(Whitescreen.Instance.render.material.DOColor(Color.clear, 4));

                    message.text = "¡Fin del juego!";
                    hiscoreText.text = "Encontraste " + score + " naranjas";

                    buttonBg.enabled = true;
                    buttonText.text = "Jugar";

                    score = 0;
                    gameDuration = originalDuration;
                    gameStarted = false;

                    t.self.Stop();
                }
            })
            .Add(1)
            .Repeat();
        }
    }


    public void PointerEnter()
    {
        // Ignore if we are playing
        if (gameStarted) return;


        // Punch
        DOTween
            .Sequence()
            .Append(button.transform.DOShakeScale(1f, 0.2f))
            .AppendCallback(() => button.localScale = buttonScale);


        // Game start countdown
        this.tt("Countdown").Add(1, () =>
        {
            message.text = "3";
        })
        .Add(1, () =>
        {
            message.text = "2";
        })
        .Add(1, () =>
        {
            message.text = "1";
        })
        .Add(1, () =>
        {
            gameStarted = true;
            buttonBg.enabled = false;

            message.text = "¡A buscar naranjas!";
            hiscoreText.text = "";

            // Flash
            DOTween
                .Sequence()
                .Append(Whitescreen.Instance.render.material.DOColor(Color.white, 0.1f))
                .Append(Whitescreen.Instance.render.material.DOColor(Color.clear, 1));


            this.tt("Game").Play();
        })
        .Immutable();
    }


    public void PointerExit()
    {
        if (!gameStarted)
        {
            this.tt("Countdown").Reset();
            message.text = "¡Observa el botón\npara iniciar!";
        }
    }
}
