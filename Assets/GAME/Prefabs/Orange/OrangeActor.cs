﻿
// @matnesis
// 2017/04/19 12:55 pm


using UnityEngine;
using DG.Tweening;
using matnesis.TeaTime;

public class OrangeActor : MonoBehaviour
{
    [Header("Config")]
    private float rebornDelay = 10;
    public float rebornScale = 31.6f;

    [Header("Required")]
    public Rigidbody rbody;
    public Transform spawner; // Special place to clone ourselves in some seconds


    void Start()
    {
        // Spawner setup
        if (spawner) spawner.SetParent(null);
        transform.SetParent(null);
    }


    public void PointerEnter()
    {
        // Ignore already fallen oranges
        if (rbody.useGravity) return;


        // Shake it on visual
        transform.DOShakeScale(0.4f, 16);


        // Drop it if the player keep seeing for a while
        this.tt("@dropIt")
            .Add(0.4f, () =>
            {
                rbody.useGravity = true;
                var r = Vector3.ClampMagnitude(Random.insideUnitSphere, 0.34f);
                rbody.AddForce((Vector3.up + r) * 96);

                // Hardcoded score
                PlayButton.Instance.score += 1;
                PlusOne.Instance.Show(transform.position);
            })
            // If the drop process happen, then create a new orange at the same
            // place
            .Add(rebornDelay, () =>
            {
                var orange = Instantiate(transform);

                // Config
                var oa = orange.GetComponent<OrangeActor>();
                oa.spawner = spawner;
                oa.rbody.useGravity = false;
                orange.position = spawner.position;

                // Pop animation
                var scale = new Vector3(rebornScale, rebornScale, rebornScale);
                // orange.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                DOTween.Sequence()
                    .Append(orange.DOShakeScale(1f, 16));
            })
            .Immutable();
    }


    public void PointerExit()
    {
        // Quick fix to the orange-banana bug :P
        if (transform.localScale.x != rebornScale)
            transform.DOScale(new Vector3(rebornScale, rebornScale, rebornScale), 1);

        // Ignore already fallen oranges
        if (rbody.useGravity) return;


        // Stop the drop process
        this.tt("@dropIt").Stop();
    }
}
